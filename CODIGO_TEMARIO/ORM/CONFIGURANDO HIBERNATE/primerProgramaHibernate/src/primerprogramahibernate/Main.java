package primerprogramahibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;

public class Main {
	public static void main(String[] args) {

		SessionFactory sesion = HibernateUtil.getSessionFactory(); 
		Session session = sesion.openSession(); //Abrimos una sesión llamando a la fábrica de sesiones
		Transaction tx = session.beginTransaction(); //Creamos una transacción

		System.out.println("Inserto una fila en la tabla DEPARTAMENTOS.");

		Departamentos dep = new Departamentos(); //Creamos un objeto para insertar un departamento
		dep.setDeptNo((byte) 60); //Establecemos los valores de los campos
		dep.setDnombre("MARKETING");
		dep.setLoc("GUADALAJARA");

		session.save(dep); //Guarda el objeto
		try {
			tx.commit(); //Confirmamos la transacción
		} catch (ConstraintViolationException e) {
			System.out.println("DEPARTAMENTO DUPLICADO");
			System.out.printf("MENSAJE: %s%n",e.getMessage());
			System.out.printf("COD ERROR: %d%n",e.getErrorCode());		
			System.out.printf("ERROR SQL: %s%n" , 
                  e.getSQLException().getMessage());
		}

		session.close(); //Cerramos la conexión
		System.exit(0);
	}
}
