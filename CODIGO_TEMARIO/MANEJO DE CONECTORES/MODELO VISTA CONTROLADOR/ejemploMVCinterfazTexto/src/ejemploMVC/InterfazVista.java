package ejemploMVC;

public interface InterfazVista {
	void setControlador(ControlConversor c);
	
	// comienza la visualización
	void arranca(); 
	
	// cantidad a convertir
	double getCantidad(); 
	
	//texto con la conversión
	void escribeCambio(String s); 
	
	// Constantes que definen las posibles operaciones:
	static final String AEUROS="Pesetas a Euros";
	static final String APESETAS="Euros a Pesetas";
}
