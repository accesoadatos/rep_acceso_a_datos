package ejemploMVC;

public class Conversor {

	private double cambio;

	public Conversor (double valorCambio) {
		// valor en la moneda de 1 euro
		cambio = valorCambio;
	}
	public double eurosAmoneda (double cantidad) {
		return cantidad * cambio;
	}
	public double monedaAeuros (double cantidad) {
		return cantidad / cambio;
	}
}
